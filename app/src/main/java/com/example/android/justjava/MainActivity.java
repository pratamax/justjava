package com.example.android.justjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    private TextView txtView2;
    private TextView txtView1;
    private TextView txtView4;
    private Button Btn1;
    private int quantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtView2 = (TextView) this.findViewById(R.id.txtView2);
        txtView1 = (TextView) this.findViewById(R.id.txtView1);
        txtView4 = (TextView) this.findViewById(R.id.txtView4);
        Btn1 = (Button) this.findViewById(R.id.Btn1);

    }

    public void onClickKurang(View view) {
        if (quantity > 0) {
            quantity--;
            txtView2.setText(quantity + "");
        }
    }

    public void onClickTambah(View view) {
        if (quantity < 10) {
            quantity++;
            txtView2.setText(quantity + "");
        }
    }


    public void onClickOrder(View view) {
        txtView4.setText("Total : $" + quantity * 5 + "\nThank You");
    }
}
